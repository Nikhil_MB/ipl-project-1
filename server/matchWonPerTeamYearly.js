// Number of matches won per team per year in IPL.
const fs = require("fs");
const csvParser = require("csv-parser");

const json = "public/output/matchWonPerTeamYearly.json";
const matchesWonPerTeam = {};

const addTeamWinsToObject = (season, winner) => {
  Object.prototype.hasOwnProperty.call(matchesWonPerTeam[season], winner)
    ? (matchesWonPerTeam[season][winner] += 1)
    : (matchesWonPerTeam[season][winner] = 1);
};

const addYearToObject = (season, winner) => {
  if (!Object.prototype.hasOwnProperty.call(matchesWonPerTeam, season)) {
    matchesWonPerTeam[season] = {};
    matchesWonPerTeam[season][winner] = 1;
  } else {
    addTeamWinsToObject(season, winner);
  }
};

const matchesWon = (csv) => {
  const reader = fs.createReadStream(csv);
  const writer = fs.createWriteStream(json);

  reader
    .on("error", (err) => console.log(err.message))
    .pipe(csvParser())
    .on("data", (data) => {
      if (data.winner) addYearToObject(data.season, data.winner);
    })
    .on("end", () => {
      writer.write(JSON.stringify(matchesWonPerTeam, null, " "));
    });
};

module.exports = matchesWon;
