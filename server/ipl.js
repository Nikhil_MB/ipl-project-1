const economicalBowlers = require("./economicalBowlers");
const extraRunsConceded = require("./extraRunsPerTeam");
const matchesPerYear = require("./matchesPerYear");
const matchesWon = require("./matchWonPerTeamYearly");

const matchesCsv = "data/matches.csv";
const deliveriesCsv = "data/deliveries.csv";
const year = "2016";
const range = 10;

const writeToJson = {
  totalMatchesPerYear: () => {
    matchesPerYear(matchesCsv);
  },

  matchesWonPerTeamYear: () => {
    matchesWon(matchesCsv);
  },

  extraRunsConcededByTeam: () => {
    extraRunsConceded(deliveriesCsv, matchesCsv, year);
  },

  economicalBowlersPerSeason: () => {
    economicalBowlers(deliveriesCsv, matchesCsv, range, "2015");
  },
};

module.exports = writeToJson;
