// Extra runs conceded per team in the year 2016
const fs = require("fs");
const csvParser = require("csv-parser");

const json = "public/output/extraRunsPerTeam.json";
const idSeason = {};
const extraRuns = {};

const extraRunsConceded = (deliveriesCsv, matchesCsv, seasons = "2016") => {
  const writer = fs.createWriteStream(json);
  const matchReader = fs.createReadStream(matchesCsv);
  const deliveryReader = fs.createReadStream(deliveriesCsv);

  matchReader
    .on("error", (err) => console.log(err.message))
    .pipe(csvParser())
    .on("data", (data) => {
      idSeason[data.id] = data.season;
    })
    .on("end", () => {
      deliveryReader
        .on("error", (err) => console.log(err.message))
        .pipe(csvParser())
        .on("data", (data) => {
          if (idSeason[data.match_id] === seasons) {
            !Object.prototype.hasOwnProperty.call(extraRuns, data.bowling_team)
              ? (extraRuns[data.bowling_team] = Number(data.extra_runs))
              : (extraRuns[data.bowling_team] += Number(data.extra_runs));
          }
        })
        .on("end", () => {
          writer.write(JSON.stringify(extraRuns, null, " "));
        });
    });
};

module.exports = extraRunsConceded;
