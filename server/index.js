// This is the entry point of the app
// Result is stored in json files

const writeToJson = require("./ipl");

//how many matches were played in every season.
writeToJson.totalMatchesPerYear();

//how many matches were won by a team yearly.
writeToJson.matchesWonPerTeamYear();

//how many extra runs are given by a team in a particular season
writeToJson.extraRunsConcededByTeam();

//top 10 economical bowlers in a particular season.
writeToJson.economicalBowlersPerSeason();
