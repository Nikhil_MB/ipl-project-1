// Top 10 economical bowlers in the year 2015

const fs = require("fs");
const csvParser = require("csv-parser");

const json = "public/output/economicalBowlers.json";
const idSeason = {};
const bowlers = {};
const bowlerDeliveries = {};
const economicBowlers = [];

function economicalBowlers(deliveriesCsv, matchesCsv, range, seasons = "2015") {
  const writer = fs.createWriteStream(json);
  const matchReader = fs.createReadStream(matchesCsv);
  const deliveryReader = fs.createReadStream(deliveriesCsv);

  matchReader
    .on("error", (err) => console.log(err.message))
    .pipe(csvParser())
    .on("data", (data) => {
      idSeason[data.id] = data.season;
    })
    .on("end", () => {
      deliveryReader
        .on("error", (err) => console.log(err.message))
        .pipe(csvParser())
        .on("data", (data) => {
          if (idSeason[data.match_id] === seasons) {
            !Object.prototype.hasOwnProperty.call(bowlers, data.bowler)
              ? (bowlers[data.bowler] =
                  Number(data.batsman_runs) +
                  Number(data.wide_runs) +
                  Number(data.noball_runs))
              : (bowlers[data.bowler] +=
                  Number(data.batsman_runs) +
                  Number(data.wide_runs) +
                  Number(data.noball_runs));

            !Object.prototype.hasOwnProperty.call(bowlerDeliveries, data.bowler)
              ? (bowlerDeliveries[data.bowler] = 1)
              : (bowlerDeliveries[data.bowler] += 1);
          }
        })
        .on("end", () => {
          const result = {};
          for (let key in bowlers) {
            if (bowlerDeliveries[key] >= 60) {
              economicBowlers.push([
                key,
                Math.round((bowlers[key] / (bowlerDeliveries[key] / 6)) * 100) /
                  100,
              ]);
            }
          }
          economicBowlers.sort((a, b) => {
            return a[1] - b[1];
          });
          for (let index = 0; index < range; index++) {
            result[economicBowlers[index][0]] = economicBowlers[index][1];
          }
          writer.write(JSON.stringify(result, null, " "));
        });
    });
}

module.exports = economicalBowlers;
