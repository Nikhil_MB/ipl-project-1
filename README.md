# IPL Data Project

This project reads data from csv files and perform various calculations on it by using different functions and produce the output into the corresponding JSON files.

## Requirements

- node version 16.0 or higher.
- npm version 7 or higher.

## how to get started

- First clone the repository.
- Then in src folder run "npm install".
- Then run "node server/index.js".

The results will be displayed in JSON files

## Modules

1. index.js (Entry point) :- It calls all the modular functions.
1. ipl.js :- It contains multiple file write methods in an object called writeToJson.
   - File write methods writes the result of the calculations into the JSON files.
   - These methods can be called by using writeToJson object.
1. matchesPerYear :- It takes one callback function and one csv file as parameters. It reads the file and calculates number of matches played in every season and store the result into an object
1. matchesWonPerTeamYearly :- It takes one callback function and one csv file as parameters. It reads the file and calculates number of matches won by a team in every season and store the result into an object
1. extraRunsPerTeam :- It takes one callback function, two csv files, and one season as parameters. It reads the files and calculates number of extra run conceded by a team in a particular season and store the result into an object.
1. economicBowlers :- It takes one callback function, two csv files,one range, and one season as parameters. It reads the files and calculates top economical bowlers of a particular season by the condition that they have atleast delivered 10 overs and store the result into an object.

## Branches

There are four branches :-

- master :- This is the main branch.
- higherOrder :- It is a copy of master branch but here modules are implemented using higher order functions.
- chaining :- It is a copy of higerOrder branch but here modules are implemented using more higher order functions.
- highCharts :- It is a copy of master branch but here additionally we can see visual representation of data by using charts

This is an initial phase of the project. Major updates are coming in the future
